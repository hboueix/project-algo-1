<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calcul routier</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
        h1 {text-align: center;}
        button {width: 100%;}
        tr {text-align: center;}
    </style>
</head>
<body>
    
    <div class="container">
        <div class="row">

            <h1 class='col-lg-12'>Programme de calcul routier</h1>
            
            <!-- Widget bonneroutes.com
            <a class="rp-widget-link" rel="noopener" target="_blank" data-currency="EUR" data-measure="metric" data-css="https://www.bonnesroutes.com/widget/v1/widget.css?pc=269adb&bc=ffffff&tc=000000" data-show-result-length data-only-countries="FR" data-prefer-countries="FR" href="https://www.bonnesroutes.com/"></a>
            <script async="async" src="https://www.bonnesroutes.com/widget/v1/client.js"></script>
            -->
        
            <form class='col-lg-12' method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                <div class='row'>
                    <div class="col-lg-6 form-group">
                        <label for="from">Depuis :</label><br>
                        <input type="text" name="from" id="from" class="form-control">
                    </div>
                    <div class="col-lg-6 form-group">
                        <label for="to">Jusqu'à :</label><br>
                        <input type="text" name="to" id="to" class="form-control">
                    </div>
                    <div class="col-md-2 offset-md-5 form-group">
                        <button class="btn btn-primary" type="submit">Calculer</button>
                    </div>
                </div>
            </form>

        </div>

        <?php
            if (isset($_POST["from"]) && isset($_POST["to"]) && !empty($_POST["from"]) && !empty($_POST["to"])) {   

                $from = $_POST["from"]; $to = $_POST["to"];
                $codesource = file_get_contents("https://www.bonnesroutes.com/widget/v1/route?defaultFrom=&defaultTo=&defaultVia=&defaultFuelConsumption=&defaultFuelPrice=&defaultSpeedLimitMotorway=&defaultSpeedLimitOther=&showVia=0&showSpeedProfile=0&showFuelCalc=0&showResultLength=1&showResultDrivingTime=0&showResultFuelAmount=0&showResultFuelCost=0&showResultCustomizedCost=0&showResultMap=0&showResultScheme=0&onlyCountries=FR&preferCountries=FR&css=https%3A%2F%2Fwww.bonnesroutes.com%2Fwidget%2Fv1%2Fwidget.css%3Fpc%3D269adb%26bc%3Dffffff%26tc%3D000000%26ff%3D-apple-system%252C%2520BlinkMacSystemFont%252C%2520%2522Segoe%2520UI%2522%252C%2520Roboto%252C%2520%2522Helvetica%2520Neue%2522%252C%2520Arial%252C%2520%2522Noto%2520Sans%2522%252C%2520sans-serif%252C%2520%2522Apple%2520Color%2520Emoji%2522%252C%2520%2522Segoe%2520UI%2520Emoji%2522%252C%2520%2522Segoe%2520UI%2520Symbol%2522%252C%2520%2522Noto%2520Color%2520Emoji%2522%26fs%3D16px&currency=EUR&measure=metric&customizedCostFormula=&customizedCostLabel=&from=" . $from . "&to=" . $to . "&v=&sm=90&so=90&fc=8.00");
                
                preg_match("#<div id=\"total_distance\">.+</div>#", $codesource, $datascraped);
                preg_match("#<div>...</div>#", $datascraped[0], $total_distance);
                
                if (isset($total_distance[0])) 
                {
                    $distance = intval(strip_tags($total_distance[0]));
                    
                    // En 18 min à 45 km/h (accéleration et freinage) -> 13.5 km
                    // En 1h42 à 90 km/h -> 153 km
                    // Donc en 2h15 (pause comprise) -> 166.5 km  
                    $d = $distance;
                    $t = 0; // temps en minutes
                    while ($d > 0) {
                        if ($d > 166.5) {
                            $d -= 166.5;
                            $t += 135;
                        } else if ($d < 166.5) {
                            if ($d <= 13.5) { 
                                $t += ($d / 45); // Vmoy = 45
                                $d = 0;
                            } else { // $d > 13.5
                                $t += 18;
                                $d -= 13.5;
                                $t += ($d / 90);
                                $d = 0;
                            }
                        } else { // $d == 166.5
                            $d -= 166.5;
                            $t += 120;
                        }
                    }
                    $time = $t * 60; // en secondes

                    date_default_timezone_set('UTC'); // important !
                    
                    echo "<table class='table'>";
                    echo "<thead><tr><th scope='col'>Départ</th><th scope='col'>Arrivée</th><th scope='col'>Distance à parcourir</th><th scope='col'>Temps de trajet estimé</th></tr></thead>";
                    echo "<tbody><tr><td>" . $from . "</td><td>" . $to . "</td><td>" . $distance . "</td><td>" . date("G:i", $time) . "</td></tr></tbody>";
                    echo "</table>";

                } else {
                    echo "Données inexistantes pour ces villes.";
                }

            }
        ?>
        
    </div>
</body>
</html>






